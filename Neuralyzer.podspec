Pod::Spec.new do |s|
  s.name = "Neuralyzer"
  s.version = "1.0.0"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.summary = "Serving up cool and refreshing neural nets with Swift"
  s.homepage = 'https://set.gl'
  s.social_media_url = "https://twitter.com/EverySet"
  s.authors = { "Set Team" => "team@set.gl" }
  s.source = { :git => "https://gitlab.com/weareset/Neuralyzer.git", :tag => "#{s.version}" }

  s.platform = 'ios'
  s.ios.deployment_target = '10.0'

  s.source_files = "Neuralyzer/*.swift"
  s.frameworks = "Accelerate"
end
