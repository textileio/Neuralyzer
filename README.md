# Neuralyzer

Serving up cool and refreshing neural networks with Swift
 
## Overview

`Neuralyzer` is a lightweight library to build and train neural networks in Swift. `Neuralyzer` is a work in progress (let's say α stage), so input is very welcome. The available documentation is extremely limited for now. The project is on [Gitlab](https://gitlab.com/weareset/Neuralyzer).

## Quick example

We'll start with a really simple example to show you how to set up and train a very basic sequential neural network. For now, we'll stick with random data, but for an example that uses real JSON data, checkout the Iris example in the package's Playground.

Start with the import and some fake data:

```swift
import Neuralyzer

// Create some random uniform data.
// Here we have four input features and two output features (targets):
let nrows = 50
let X = Matrix(Vector(initializedWith: uniform, count: nrows * 4),
               rows: nrows,
               columns: 4)
let Y = Matrix(Vector(initializedWith: uniform, count: nrows * 2),
               rows: nrows,
               columns: 2)
```

Next we setup the model architecture. Models are responsible for structuring layers and prediction... That's it! So we setup a basic neural network with four inputs, and two hidden layers: one with eight units and another (the output layer) with two. We also need to setup a loss function and an optimizer object. These are used in training the sequential model.

```swift
let loss = CategoricalCrossentropy()  // Useful for classification
let optim = RMSProp(loss: loss)  // Usually defaults are okay

let net = Sequential(inputSize: 4, optimizer: optim)
net.add(layer: Dense(units: 8, activation: ReLU()))
net.add(layer: Dense(units: 2, activation: Softmax()))
```

Now we start the training process. The following is a pretty typical setup, but other variations are possible. We show a step decay here in case that's something you'd want to use (i.e., decrease your learningRate over your epochs:

```swift
let epochs = 20      // How many training runs do you want?
let stepDecay = 0.9  // Use this to decay learningRate over epochs
let batchSize = 10   // How many inputs per batch?
for epoch in 0..<epochs {
  let t0 = Date()    // Keep track of timing (too slow right now!)
  var last = 0
  for now in stride(from: 9, to: X.rows, by: batchSize) {
    let x = X[last...now, 0..<4]
    let y = Y[last...now, 0..<2]
    net.train(input: x, target: y)
    last = now
  }
  optim.learningRate *= stepDecay // Update each epoch
  // Evaluate model fit. We're using the training set, don't do this!
  let estimate = net.predict(input: X) // Grab estimates for all of X
  let l = loss.fx(input: estimate, target: Y) // Compute loss at current epoch
  let accuracy = evaluate(estimate: estimate, target: Y)
  let time = Date().timeIntervalSince(t0)  // How long did that take?
  // Let's track our progress!
  print("Epoch \(epoch+1)/\(epochs)")
  print("\(time)s - loss: \(l) - accuracy \(accuracy)")
}
```

And that's pretty much it! We've now training a pretty basic neural network on totally random data. Doesn't tell us much about anything, but it gives us a pretty good idea of how to use `Neuralyzer` once you have some real data to model.

Happy `Neuralyzing`!
