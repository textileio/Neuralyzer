import Foundation
import Accelerate

//infix operator ×=
//infix operator ×
//
//public func ×=<ML: MutableQuadraticType, MR: QuadraticType>(lhs: inout ML, rhs: MR) where ML.Element == Float, MR.Element == Float {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise multiplication")
//  assert(lhs.span ≅ rhs.span)
//
//  for (lhsIndex, rhsIndex) in zip(lhs.span, rhs.span) {
//    lhs[lhsIndex] *= rhs[rhsIndex]
//  }
//}
//
//public func ×<ML: QuadraticType, MR: QuadraticType>(lhs: ML, rhs: MR) -> Matrix<Float> where ML.Element == Float, MR.Element == Float {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise multiplication")
//
//  var results = Matrix<Float>(lhs)
//  results ×= rhs
//
//  return results
//}
//
//public func ×=<ML: MutableQuadraticType, MR: QuadraticType>(lhs: inout ML, rhs: MR) where ML.Element == Double, MR.Element == Double {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise multiplication")
//  assert(lhs.span ≅ rhs.span)
//
//  for (lhsIndex, rhsIndex) in zip(lhs.span, rhs.span) {
//    lhs[lhsIndex] *= rhs[rhsIndex]
//  }
//}
//
//public func ×<ML: QuadraticType, MR: QuadraticType>(lhs: ML, rhs: MR) -> Matrix<Double> where ML.Element == Double, MR.Element == Double {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise multiplication")
//
//  var results = Matrix<Double>(lhs)
//  results ×= rhs
//
//  return results
//}
//
//public func /=<ML: MutableQuadraticType, MR: QuadraticType>(lhs: inout ML, rhs: MR) where ML.Element == Float, MR.Element == Float {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise division")
//  assert(lhs.span ≅ rhs.span)
//
//  for (lhsIndex, rhsIndex) in zip(lhs.span, rhs.span) {
//    lhs[lhsIndex] /= rhs[rhsIndex]
//  }
//}
//
//public func /<ML: QuadraticType, MR: QuadraticType>(lhs: ML, rhs: MR) -> Matrix<Float> where ML.Element == Float, MR.Element == Float {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise division")
//
//  var results = Matrix<Float>(lhs)
//  results /= rhs
//
//  return results
//}
//
//public func /=<ML: MutableQuadraticType, MR: QuadraticType>(lhs: inout ML, rhs: MR) where ML.Element == Double, MR.Element == Double {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise division")
//  assert(lhs.span ≅ rhs.span)
//
//  for (lhsIndex, rhsIndex) in zip(lhs.span, rhs.span) {
//    lhs[lhsIndex] /= rhs[rhsIndex]
//  }
//}
//
//public func /<ML: QuadraticType, MR: QuadraticType>(lhs: ML, rhs: MR) -> Matrix<Double> where ML.Element == Double, MR.Element == Double {
//  try! precondition(lhs.rows == rhs.rows && lhs.columns == rhs.columns, "Matrix dimensions not compatible with element-wise division")
//
//  var results = Matrix<Double>(lhs)
//  results /= rhs
//
//  return results
//}
//
//public func /=<ML: MutableQuadraticType>(lhs: inout ML, rhs: Double) where ML.Element == Double {
//  for index in lhs.span {
//    lhs[index] /= rhs
//  }
//}
//
//public func /<MR: QuadraticType>(lhs: MR, rhs: Double) -> Matrix<Double> where MR.Element == Double {
//  var results = Matrix<Double>(lhs)
//  results /= rhs
//  return results
//}
//
//public func +=<ML: MutableQuadraticType>(lhs: inout ML, rhs: Double) where ML.Element == Double {
//  for index in lhs.span {
//    lhs[index] += rhs
//  }
//}
//
//public func +<MR: QuadraticType>(lhs: MR, rhs: Double) -> Matrix<Double> where MR.Element == Double {
//  var results = Matrix<Double>(lhs)
//  results += rhs
//  return results
//}
//
//public func +<MR: QuadraticType>(lhs: Double, rhs: MR) -> Matrix<Double> where MR.Element == Double {
//  var results = Matrix<Double>(rhs)
//  results += lhs
//  return results
//}
//
//public func argmax<M: LinearType>(_ x: M) -> Int where M.Element == Double {
//  var value: Double = 0.0
//  var index: vDSP_Length = 0
//  withPointer(x) { xp in
//    vDSP_maxviD(xp + x.startIndex, x.step, &value, &index, vDSP_Length(x.count))
//  }
//  return Int(index)
//}
//
//public func argmax<M: LinearType>(_ x: M) -> Int where M.Element == Float {
//  var value: Float = 0.0
//  var index: vDSP_Length = 0
//  withPointer(x) { xp in
//    vDSP_maxvi(xp + x.startIndex, x.step, &value, &index, vDSP_Length(x.count))
//  }
//  return Int(index)
//}
//
//// Compute `x^alpha` for each element of `x`, return a new `ValueArray` with the results
//public func pow<M: LinearType>(_ x: M, _ y: Float) -> ValueArray<Float> where M.Element == Float {
//  try! precondition(x.step == 1, "pow doesn't support step values other than 1")
//  let z = ValueArray<Float>(count: x.count, repeatedValue: y)
//  return pow(z, x as! ValueArray<Float>)
//}
//
//// Compute `x^alpha` for each element of `x`, return a new `ValueArray` with the results
//public func pow<M: LinearType>(_ x: M, _ y: Double) -> ValueArray<Double> where M.Element == Double {
//  try! precondition(x.step == 1, "pow doesn't support step values other than 1")
//  let z = ValueArray<Double>(count: x.count, repeatedValue: y)
//  return pow(z, x as! ValueArray<Double>)
//}
//
// Compute sigmoid function with automatic 'clipping' between -500 and +500
internal func expit(_ x: Vector<Double>) -> Vector<Double> {
  let signal = clip(x, low: -500.0, high: 500.0)  // Prevent overflow
  return rec(exp(neg(signal)) + 1.0)
}

// Compute sigmoid function with automatic 'clipping' between -500 and +500
internal func expit(_ x: Vector<Float>) -> Vector<Float> {
  let signal = clip(x, low: -500.0, high: 500.0)  // Prevent overflow
  return rec(exp(neg(signal)) + 1.0)
}
//
//public func rowWise<M: QuadraticType>(_ x: M, using function: (_ v: ValueArray<Double>) ->
//  ValueArray<Double> ) -> Matrix<Double> where M.Element == Double {
//
//  let inputMatrix = Matrix<Double>(x)
//  var individualVectors: [[Double]] = []
//  for i in 0..<inputMatrix.rows {
//    let vector = ValueArray(inputMatrix.row(i))
//    let result = [Double](function(vector))
//    individualVectors.append(result)
//  }
//  // Create a new matrix that will hold the individual rows
//  return Matrix(individualVectors)
//}
//
//public func apply<ML: QuadraticType>(_ lhs: ML, fun: (_ v: ValueArray<Double>) -> ValueArray<Double>) ->
//  Matrix<Double> where ML.Element == Double {
//    let results = Matrix<Double>(lhs)
//    return Matrix<Double>(rows: lhs.rows, columns: lhs.columns,
//                          elements: fun(results.elements))
//}

func uniform() -> Double {
  let ARC4RANDOM_MAX: Double = 0x100000000
  let range = 2.0
  return Double(arc4random()) / ARC4RANDOM_MAX * range - 1.0
}

func uniform(low: Double = -1.0, high: Double = 1.0) -> Double {
  let ARC4RANDOM_MAX: Double = 0x100000000
  let range = high - low
  return Double(arc4random()) / ARC4RANDOM_MAX * range + low
}

//func calculateIndex(columns: Int, y: Int, x: Int) -> Int {
//  return y * columns + x
//}
//
//func calculateCoordinates(_ index: Int, rows: Int, columns: Int) -> (Int, Int)! {
//  for i in (0..<rows) {  // For each row
//    // Check if the index parameter is in the row
//    if (index < (columns * i) + columns && index >= columns * i) {
//      return (i: i, j: index - columns * i)  // Return x, y
//    }
//  }
//  return nil
//}
//
//func kronecker(_ i: Int, _ j: Int) -> Double {
//  return i == j ? 1.0 : 0.0
//}
//
//func kron(rows: Int, columns: Int) -> Matrix<Double> {
//  let elements = ValueArray<Double>((0..<(rows*columns)).map{el in
//    let coordinates = calculateCoordinates(el, rows: rows, columns: columns)
//    return kronecker((coordinates?.0)!, (coordinates?.1)!)
//  })
//  return Matrix<Double>(rows: rows, columns: columns, elements: elements)
//}

extension Dictionary {

  mutating func merge(with dictionary: Dictionary) {
    dictionary.forEach { updateValue($1, forKey: $0) }
  }

  func merged(with dictionary: Dictionary) -> Dictionary {
    var dict = self
    dict.merge(with: dictionary)
    return dict
  }
}
