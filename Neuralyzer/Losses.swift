import Foundation

// TODO: Add CategoricalHinge, KullbackLeiblerDivergence, Poisson, CosineDistance

// TODO: Maybe update these to just functions with a variable number of inputs
/// and at least one 'withGradient' parameter?
/// Any 'function' that takes two input Matrixes and returns a loss Double
protocol DifferentiableLoss: Specable {
  var ϵ: Double { get }
  /// Main loss function
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean loss between input and target
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double
  /// Gradient/derivative of loss function
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double>
}

extension DifferentiableLoss {
  var ϵ: Double { return 1e-10 }
}

/// Mean squared error
struct MeanSquaredError: DifferentiableLoss {
  /// Mean squared error
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean squared error (aka quadratic) loss
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    // We compute 1/2 mean loss to avoid multiplying gradient by 2
    return mean(pow((input.grid - target.grid), 2.0)) / 2.0
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid - target.grid
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Mean absolute error
struct MeanAbsoluteError: DifferentiableLoss {
  /// Mean absolute error
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean squared error (aka quadratic loss)
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    return mean(abs(input.grid - target.grid))
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    // Create array of ones to take on signs from copysign function
    let ones = Vector<Double>(repeating: 1, count: input.grid.count)
    // Use copysign to set the sign of each value in ones based on the
    // difference between input and target
    let elements = copysign(ones, magnitude: input.grid - target.grid)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Squared hinge error
struct SquaredHinge: DifferentiableLoss {
  /// Squared hinge error
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean squared hinge error
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    let prod = input.grid * target.grid
    return mean(pow(threshold(neg(prod) + 1.0, low: 0.0), 2.0))
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let prod = input.grid * target.grid
    let diff = input.grid - target.grid
    // TODO: Find a more efficient way to perform this (vectorized?)
    // where(prod < 1.0, 2.0*(target - input), 0.0)
    let elements = zip(prod, diff).map { p, d in p < 1.0 ? 2.0 * d : 0.0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Hinge error
struct Hinge: DifferentiableLoss {
  /// Hinge error
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean hinge error
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    let prod = input.grid * target.grid
    // TODO: Find a more efficient way to perform this (vectorized?)
    return mean(threshold(neg(prod) + 1.0, low: 0.0))
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let prod = input.grid * target.grid
    // TODO: Find a more efficient way to perform this (vectorized?)
    // where(prod < 1.0, 2.0*(target - input), 0.0)
    let elements = zip(prod, target.grid).map { $0 < 1.0 ? -$1 : 0.0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Logcosh error
struct Logcosh: DifferentiableLoss {
  /// Logcosh error
  ///
  /// Similar to Huber loss, but twice-differentiable everywhere
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean logcosh error
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    return mean(log(cosh(input.grid - target.grid)))
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let elements = sinh(input.grid - target.grid)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Poisson loss
struct Poisson: DifferentiableLoss {
  /// Poisson error
  ///
  /// Useful for count-based outcomes. Assumes non-negative outcome values.
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean Poisson error
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    let prediction: Vector<Double> = input.grid
    return mean(prediction - target.grid * log(prediction + ϵ))
  }

  /// Gradient/derivative of loss
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let elements = neg(target.grid)/(input.grid + ϵ) + 1.0
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Binary crossentropy loss
struct BinaryCrossentropy: DifferentiableLoss {
  /// Binary crossentropy loss (aka logistic loss, log loss)
  ///
  /// The signals should be in the range [0, 1]
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean binary crossentropy loss
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    // Prevent overflow
    let clipped = clip(input.grid, low: ϵ, high: 1.0-ϵ)
    let targets = target.grid
    let right = target.grid * log(clipped)
    let wrong = (neg(targets) + 1.0) * log(neg(clipped) + 1.0)
    return mean(neg(right + wrong))
  }

  /// Gradient/derivative of loss
  ///
  /// The signals should be in the range [0, 1]
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    // Prevent overflow
    let clipped = clip(input.grid, low: ϵ, high: 1.0 - ϵ)
    let divisor = threshold(clipped * (neg(clipped) + 1.0), low: ϵ)
    let elements = (clipped - target.grid) / divisor
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

/// Categorical crossentropy loss
struct CategoricalCrossentropy: DifferentiableLoss {
  /// Categorical crossentropy loss
  ///
  /// The signals should be 2D Matrixs with values the range [0, 1]
  ///
  /// - Parameters:
  ///   - input: Expected or predicted input Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Mean categorical crossentropy loss
  func fx(input: Matrix<Double>, target: Matrix<Double>) -> Double {
    // Prevent overflow by scaling
    var total = 0.0
    for i in 0..<input.rows {
      let row = input[row: i]
      let rowSum = sum(row)
      let scaled = clip(row/rowSum, low: ϵ, high: 1.0 - ϵ)
      total += -sum(target[row: i] * log(scaled))
    }
    return total/Double(input.rows)
  }

  /// Gradient/derivative of loss
  ///
  /// The signals should be 2D Matrixs with values the range [0, 1]
  ///
  /// - Parameters:
  ///   - input: Expected or predicted output Matrix
  ///   - target: Observed or target output Matrix
  /// - Returns: Result of fx' wrt inputs as a Matrix
  func prime(input: Matrix<Double>, target: Matrix<Double>) -> Matrix<Double> {
    let pred = Matrix(copy: input)
    for i in 0..<pred.rows {
      var row = pred[row: i]
      row = (row / sum(row))
    }
    // Prevent overflow by scaling
    let elements = clip(pred.grid, low: ϵ, high: 1.0 - ϵ)
    return Matrix<Double>(elements - target.grid, rows: input.rows, columns: input.columns)
  }
}
