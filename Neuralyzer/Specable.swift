import Foundation

// Any object who's specification is defined by type and configuration
protocol Specable {
  /// Type (class name) of NN component (e.g., Layer, Optimizer, etc)
  var type: String { get }
  /// Configuration of sub-components of NN component
  var config: [String: Any] { get }
  /// Overall component specification (i.e., combination of type and config)
  var spec: [String: Any] { get }
}

/// Provide default implementation for all Specable objects
extension Specable {
  var type: String {
    return String(describing: Swift.type(of: self)).lowercased()
  }
  var spec: [String: Any] {
    return ["class_name": type, "config": config]
  }
  var config: [String: Any] {
    return [:]
  }
}

// MARK: Activations

extension Softmax: Specable {}

extension ELU: Specable {
  var config: [String : Any] {
    return ["alpha": alpha]
  }
}

extension Softplus: Specable {}

extension Softsign: Specable {}

extension ReLU: Specable {}

extension LeakyReLU: Specable {
  var config: [String : Any] {
    return ["alpha": alpha]
  }
}

extension Tanh: Specable {}

extension Sigmoid: Specable {}

extension Linear: Specable {}

extension HardSigmoid: Specable {}

extension Elliot: Specable {
  var config: [String : Any] {
    return ["alpha": alpha]
  }
}

extension SymmetricElliot: Specable {
  var config: [String : Any] {
    return ["alpha": alpha]
  }
}

// MARK: Initializers

extension External: Specable {}

extension Zeros: Specable {}

extension Ones: Specable {}

extension Constant: Specable {
  var config: [String : Any] {
    return ["value": value.description]
  }
}

extension RandomUniform: Specable {
  var config: [String : Any] {
    return ["minval": low.description,
            "maxval": high.description,
            "seed": seed?.description ?? "none"]
  }
}

extension RandomNormal: Specable {
  var config: [String : Any] {
    return ["mean": mean.description, "stddev": stddev.description,
            "seed": seed?.description ?? "none"]
  }
}

extension TruncatedNormal: Specable {}

extension ScaledUniform: Specable {
  var config: [String : Any] {
    return ["scale": scale.description,
            "distribution": "uniform",
            "mode": String(describing: mode),
            "seed": seed?.description ?? "none"]
  }
}

extension ScaledNormal: Specable {
  var config: [String : Any] {
    return ["scale": scale.description,
            "distribution": "normal",
            "mode": String(describing: mode),
            "seed": seed?.description ?? "none"]
  }
}

// MARK: Layers

extension Dense: Specable {
  var config: [String : Any] {
    return ["activation": activation.spec,
            "name": name,
            "activity_regularizer": "none",
            "bias_initializer": biasInitializer.spec,
            "bias_regularizer": "none",
            "kernel_initializer": kernelInitializer.spec,
            "kernel_regularizer": "none",
            "bias_constraint": "none",
            "kernel_constraint": "none",
            "trainable": "true",
            "units": outputSize.description,
            "use_bias": useBias.description]
  }
}

// MARK: Losses

extension MeanSquaredError: Specable {}

extension MeanAbsoluteError: Specable {}

extension SquaredHinge: Specable {}

extension Hinge: Specable {}

extension Logcosh: Specable {}

extension Poisson: Specable {}

extension BinaryCrossentropy: Specable {}

extension CategoricalCrossentropy: Specable {}

// MARK: Models

extension Sequential: Specable {
  var config: [String: Any] {
    return ["layers": layers.map { $0.config }]
  }
}

// MARK: Optimizers

extension SGDMomentum {
  var config: [String: Any] {
    return ["lr": learningRate.description,
            "decay": decayRate.description,
            "momentum": momentum.description,
            "nesterov": nesterov.description]

  }
}

extension RMSProp {
  var config: [String: Any] {
    return ["lr": learningRate.description,
            "decay": decayRate.description,
            "rho": rho.description,
            "epsilon": ε.description]
  }
}

extension Adagrad {
  var config: [String: Any] {
    return ["lr": learningRate.description,
            "decay": decayRate.description,
            "epsilon": ε.description]
  }
}

extension Adadelta {
  var config: [String: Any] {
    return ["lr": learningRate.description,
            "decay": decayRate.description,
            "rho": rho.description,
            "epsilon": ε.description]
  }
}

extension Adam {
  var config: [String: Any] {
    return ["lr": learningRate.description,
            "decay": decayRate.description,
            "rho": rho.description,
            "epsilon": ε.description,
            "beta_1": β1.description,
            "beta_2": β2.description]
  }
}
