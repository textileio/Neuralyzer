import Foundation

protocol Layer: Specable {
  var name: String { get }
  var inputSize: Int { get }
  var outputSize: Int { get }
  var useBias: Bool { get }
  var weights: Matrix<Double>? { get }
  var id: String { get }

  func initialize(inputSize: Int, id: String, weights: Matrix<Double>?)
  func forward(input: Matrix<Double>) -> Matrix<Double>
  func activate(input: Matrix<Double>, withGradient: Bool) -> Matrix<Double>
  func loss(input: Matrix<Double>) -> Matrix<Double>
  func update(delta: Matrix<Double>)
}

extension Layer {
  var name: String {
    return type
  }
}

// Just your regular densely-connected NN layer
class Dense: Layer {
  private(set) var inputSize: Int = 0
  let outputSize: Int
  let useBias: Bool
  private(set) var weights: Matrix<Double>?
  private(set) var id: String = ""

  let activation: DifferentiableActivation
  let kernelInitializer: Initializer
  let biasInitializer: Initializer

  init(units: Int,
       activation: DifferentiableActivation = Linear(),
       useBias: Bool = true,
       kernelInitializer: Initializer = GlorotUniform(),
       biasInitializer: Initializer = Zeros()) {
    self.outputSize = units
    self.activation = activation
    self.useBias = useBias
    self.kernelInitializer = kernelInitializer
    self.biasInitializer = biasInitializer
  }

  func forward(input: Matrix<Double>) -> Matrix<Double> {
    guard let weights = weights else { return input }
    if !useBias {
      return input * weights
    }
    // Grab all rows from 1 down and all columns -> weights matrix
    let w = weights[1..<weights.rows, 0..<weights.columns]
    // Grab all columns from first row -> implicit bias
    // Then use tile to 'broadcast' bias to all rows
    let b: Vector<Double> = weights[0..<1, 0..<weights.columns].grid
    return apply({ (a: Vector<Double>) in a + b }, to: input * w, along: .rows) // Matrix multiply with bias addition
  }

  func activate(input: Matrix<Double>, withGradient: Bool = false) -> Matrix<Double> {
    return withGradient ? activation.prime(input: input) : activation.fx(input: input)
  }

  func loss(input: Matrix<Double>) -> Matrix<Double> {
    guard let weights = weights else { return input }
    if !useBias {
      return weights * input  // Basic matrix multiplication
    }
    return weights[1..<weights.rows, 0..<weights.columns] * input  // Skip the bias weight
  }

  func initialize(inputSize: Int, id: String, weights: Matrix<Double>? = nil) {
    self.inputSize = inputSize
    self.id = id

    if let weights = weights {
      self.weights = weights
      return
    }

    let extra = (useBias ? 1 : 0)
    // In following, +1 is for bias
    var newWeights = kernelInitializer.initialize(inputs: inputSize + extra, outputs: outputSize)
    if useBias {
      // TODO: This no longer has to return a Matrix
      let biases = biasInitializer.initialize(inputs: 1, outputs: outputSize)
      newWeights[row: 0] = biases.grid
    }
    self.weights = newWeights
  }

  func update(delta: Matrix<Double>) {
    guard let w = weights else { return }
    weights = w + delta
  }

}
