#import <UIKit/UIKit.h>

//! Project version number for Neuralyzer.
FOUNDATION_EXPORT double NeuralyzerVersionNumber;

//! Project version string for Neuralyzer.
FOUNDATION_EXPORT const unsigned char NeuralyzerVersionString[];
