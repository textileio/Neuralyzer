import Foundation

protocol Initializer: Specable {
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double>
}

/// Enum containing different ways of counting input/output neurons
///
/// - fanIn: Count number of input units in the weight tensor
/// - fanOut: Count number of output units in the weight tensor
/// - fanAvg: Compute average of the numbers of input and output units
enum FanType {
  case fanIn
  case fanOut
  case fanAvg

  /// Determine size of tensor/matrix given mode type
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  /// - Returns: Required size of output tensor/matrix
  func getSize(inputs: Int, outputs: Int) -> Double {
    switch self {
    case .fanIn:
      return Double(inputs)
    case .fanOut:
      return Double(outputs)
    case .fanAvg:
      return Double(inputs + outputs) / 2.0
    }
  }
}

/// Initializer that loads existing tensors/matrices
/// In this case, the tensor/matrix has already been initialized (and
/// possibly updated). This is just a 'stub' initializer to allow specifying
/// arbitrary weights or using previously learned weights and biases.
struct External: Initializer {
  let external: Matrix<Double>

  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    guard inputs == external.rows && outputs == external.columns else {
      fatalError("External initializer found row or column mismatch")
    }
    return external
  }
}

/// Initializer that generates tensors/matrices initialized to 0
struct Zeros: Initializer {
  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    return Matrix<Double>(repeating: 0, rows: inputs, columns: outputs)
  }
}

/// Initializer that generates tensors/matrices initialized to 1
struct Ones: Initializer {
  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    return Matrix<Double>(repeating: 1, rows: inputs, columns: outputs)
  }
}

/// Initializer that generates tensors/matrices initialized to a constant
struct Constant: Initializer {
  let value: Double
  /// Initialize the initializer!
  ///
  /// - Parameter value: The value of the generator tensors/matrices
  init(value: Double = 0.0) {
    self.value = value
  }
  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    return Matrix<Double>(repeating: value, rows: inputs, columns: outputs)
  }
}

/// Initializer that generates tensors with a uniform distribution
class RandomUniform: Initializer {
  let low: Double
  let high: Double
  let seed: Int?
  /// Initialize the initializer!
  ///
  /// - Parameters:
  ///   - low: Lower bound of the range of random values to generate
  ///   - high: Upper bound of the range of random values to generate
  ///   - seed: Used to seed the random generator
  init(low: Double = -0.05, high: Double = 0.05, seed: Int? = nil) {
    self.low = low
    self.high = high
    self.seed = seed
  }
  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    let dist = Distributions.Uniform(a: low, b: high)
    if let seed = seed {
      dist.seed(seed)
    }
    return Matrix<Double>(dist.Random(inputs * outputs), rows: inputs, columns: outputs)
  }
}

/// Initializer that generates tensors with a normal distribution
class RandomNormal: Initializer {
  let mean: Double
  let stddev: Double
  let seed: Int?
  /// Initialize the initializer!
  ///
  /// - Parameters:
  ///   - mean: Mean of the random values to generate
  ///   - stddev: Standard deviation of the random values to generate
  ///   - seed: Used to seed the random generator
  init(mean: Double = 0.0, stddev: Double = 0.05, seed: Int? = nil) {
    self.mean = mean
    self.stddev = stddev
    self.seed = seed
  }
  /// Initialize weights/biases
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    let dist = Distributions.Normal(m: mean, v: pow(stddev, 2.0))
    if let seed = seed {
      dist.seed(seed)
    }
    return Matrix<Double>(dist.Random(inputs * outputs), rows: inputs, columns: outputs)
  }
}

/// Initializer that generates a truncated normal distribution
class TruncatedNormal: Initializer {
  let mean: Double
  let stddev: Double
  let seed: Int?
  /// Initialize the initializer!
  ///
  /// - Parameters:
  ///   - mean: Mean of the random values to generate
  ///   - stddev: Standard deviation of the random values to generate
  ///   - seed: Used to seed the random generator
  init(mean: Double = 0.0, stddev: Double = 0.05, seed: Int? = nil) {
    self.mean = mean
    self.stddev = stddev
    self.seed = seed
  }
  /// Initialize weights/biases
  ///
  /// These values are similar to values from a `RandomNormal`
  /// except that values more than two standard deviations from the mean
  /// are discarded and re-drawn. This is the recommended initializer for
  /// neural network weights and filters.
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    if let seed = seed {
      srand48(seed)
    }
    let data = InitializerUtils.truncatedNormal(size: inputs * outputs, μ: mean, σ: pow(stddev, 2.0))
    return Matrix<Double>(data, rows: inputs, columns: outputs)
  }
}

/// Random uniform initializer capable of adapting its scale to the shape of weights
class ScaledUniform: Initializer {
  let scale: Double
  let mode: FanType
  let seed: Int?
  /// Initialize the initializer!
  ///
  /// - Parameters:
  ///   - scale: Scaling factor (positive double)
  ///   - mode: One of .fanIn, .fanOut, or .fanAvg
  ///   - seed: Used to seed the random number generator
  init(scale: Double = 1.0, mode: FanType, seed: Int? = nil) {
    self.scale = scale
    self.seed = seed
    self.mode = mode
  }
  /// Initialize weights/biases
  ///
  /// Samples are drawn from a uniform distribution within [-limit, limit],
  /// with limit = sqrt(3 * scale / n)`, where n is:
  /// - number of input units in the weight tensor, if mode = .fanIn
  /// - number of output units, if mode = .fanOut
  /// - average of the numbers of input and output units, if mode = .fanAvg
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    let scale = self.scale / Double(max(1, mode.getSize(inputs: inputs, outputs: outputs)))
    let limit = sqrt(3.0 * scale)
    let dist = Distributions.Uniform(a: -limit, b: limit)
    if let seed = seed {
      dist.seed(seed)
    }
    return Matrix<Double>(dist.Random(inputs * outputs), rows: inputs, columns: outputs)
  }
}

/// Random normal initializer capable of adapting its scale to the shape of weights
class ScaledNormal: Initializer {
  let scale: Double
  let mode: FanType
  let seed: Int?
  /// Initialize the initializer!
  ///
  /// - Parameters:
  ///   - scale: Scaling factor (positive double)
  ///   - mode: One of .fanIn, .fanOut, or .fanAvg
  ///   - seed: Used to seed the random number generator
  init(scale: Double = 1.0, mode: FanType, seed: Int? = nil) {
    self.scale = scale
    self.seed = seed
    self.mode = mode
  }
  /// Initialize weights/biases
  ///
  /// Samples are drawn from a truncated normal distribution centered on zero,
  /// with stddev = sqrt(scale / n)`, where n is:
  /// - number of input units in the weight tensor, if mode = .fanIn
  /// - number of output units, if mode = .fanOut
  /// - average of the numbers of input and output units, if mode = .fanAvg
  ///
  /// - Parameters:
  ///   - inputs: Number of input units in the weight tensor/matrix
  ///   - outputs: Number of output units in the weight tensor/matrix
  func initialize(inputs: Int, outputs: Int) -> Matrix<Double> {
    let stddev = scale / max(1.0, mode.getSize(inputs: inputs, outputs: outputs))
    if let seed = seed {
      srand48(seed)
    }
    let data = InitializerUtils.truncatedNormal(size: inputs*outputs, μ: 0.0, σ: stddev)
    return Matrix<Double>(data, rows: inputs, columns: outputs)
  }
}

/// LeCun uniform initializer
class LecunUniform: ScaledUniform {
  /// Initialize the initializer!
  ///
  /// Draws samples from a uniform distribution within [-limit, limit]
  /// where `limit` is `sqrt(3 / n)`, where `n` is the number of input (.fanIn)
  /// units in the weight tensor.
  ///
  /// - Reference: LeCun 98, Efficient Backprop
  ///   http://yann.lecun.com/exdb/publis/pdf/lecun-98b.pdf
  ///
  /// - Parameter seed: Used to seed the random generator
  init(seed: Int? = nil) {
    super.init(scale: 1.0, mode: .fanIn, seed: seed)
  }
}

/// Glorot normal initializer (aka Xavier normal initializer)
class GlorotNormal: ScaledNormal {
  /// Initialize the initializer!
  ///
  /// Draws samples from a truncated normal distribution centered on zero
  /// with `stddev = sqrt(2 / (n + m))`, where `n` is the number of input
  /// (.fanIn) units and `m` is the number of output (.fanOut) units in the
  /// weight tensor.
  ///
  /// - Reference: Glorot & Bengio, AISTATS 2010
  ///   http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf
  ///
  /// - Parameter seed: Used to seed the random generator
  init(seed: Int? = nil) {
    super.init(scale: 1.0, mode: .fanAvg, seed: seed)
  }
}

/// Glorot uniform initializer (aka Xavier uniform initializer)
class GlorotUniform: ScaledUniform {
  /// Initialize the initializer!
  ///
  /// Draws samples from a uniform distribution within [-limit, limit] where
  /// `limit` is `sqrt(6 / (n + m))`, where `n` is the number of input
  /// (.fanIn) units and `m` is the number of output (.fanOut) units in the
  /// weight tensor.
  ///
  /// - Reference: Glorot & Bengio, AISTATS 2010
  ///   http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf
  ///
  /// - Parameter seed: Used to seed the random generator
  init(seed: Int? = nil) {
    super.init(scale: 1.0, mode: .fanAvg, seed: seed)
  }
}

/// He normal initializer
class HeNormal: ScaledNormal {
  /// Initialize the initializer!
  ///
  /// Draws samples from a truncated normal distribution centered on zero
  /// with `stddev = sqrt(2 / n)`, where `n` is the number of input
  /// (.fanIn) units in the weight tensor.
  ///
  /// - Reference: He et al., http://arxiv.org/abs/1502.01852
  ///
  /// - Parameter seed: Used to seed the random generator
  init(seed: Int? = nil) {
    super.init(scale: 2.0, mode: .fanIn, seed: seed)
  }
}

/// He uniform initializer
class HeUniform: ScaledUniform {
  /// Initialize the initializer!
  ///
  /// Draws samples from a uniform distribution within [-limit, limit] where
  /// `limit` is `sqrt(6 / n)`, where `n` is the number of input
  /// (.fanIn) units in the weight tensor.
  ///
  /// - Reference: He et al., http://arxiv.org/abs/1502.01852
  ///
  /// - Parameter seed: Used to seed the random generator
  init(seed: Int? = nil) {
    super.init(scale: 2.0, mode: .fanIn, seed: seed)
  }
}

struct InitializerUtils {
  /// Draw random values from a truncated normal distribution
  ///
  /// Uses Algorithm 1: Sampling Inversion of numerical approximation to CDF
  /// from Machine Learning: a Probabilistic Perspective by Kevin Patrick Murphy
  /// which is very efficient for truncation bounds a within a few standard
  /// deviations of the mean (less than ~5 standard deviations).
  /// Effectively 'rejects' sampled points outside +/- scale*σ
  ///
  /// - Reference: Sampling From Truncated Normal
  ///   http://web.michaelchughes.com/research/sampling-from-truncated-normal
  ///
  /// - Parameters:
  ///   - size: The number of samples to draw (really only 'valid' for scale < 5.0)
  ///   - μ: The mean of the underlying normal distribution
  ///   - σ: The standard deviation of the underlying normal distribution
  ///   - scale: The truncation range (+/- scale) centered at mean
  /// - Returns: ValueArray of random values from truncated normal distribution
  static func truncatedNormal(size: Int, μ: Double = 0.0, σ: Double = 1.0, scale: Double = 2.0) -> Vector<Double> {
    try! precondition(scale < 5.0, "Scale less than 5.0 required")
    let s = (scale - μ) / σ
    let n = Distributions.Normal(m: 0.0, v: 1.0)
    let u = Distributions.Uniform(a: n.Cdf(-s), b: n.Cdf(s))
    let y = u.Random(size).map {
      n.Quantile($0)
    }
    return Vector<Double>(y * σ + μ)
  }
}
