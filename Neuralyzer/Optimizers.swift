// TODO: implement gradient clipping?
// TODO: implement regularization?
// TODO: Figure out why this is sooooo slow to compile (type inference issues)?

import Foundation

protocol Optimizer: Specable {
  var loss: DifferentiableLoss { get }
  var learningRate: Double { get set }
  var decayRate: Double { get }
  var initialDecay: Double { get }
  var id: String { get }
  var cache: [[Matrix<Double>]] { get }
  var iterations: Int { get }

  func initialize(iterations: Int, cache: [[Matrix<Double>]])
  func train(layers: [Layer], input: Matrix<Double>, target: Matrix<Double>)
}

class SGD: Optimizer {
  let loss: DifferentiableLoss
  var learningRate: Double
  let decayRate: Double
  let initialDecay: Double
  let id: String
  fileprivate(set) var cache: [[Matrix<Double>]] = []
  private(set) var iterations: Int = 0

  init(loss: DifferentiableLoss, learningRate: Double = 0.01, decayRate: Double = 0.0, initialDecay: Double = 0.0, id: String = UUID().uuidString) {
    self.loss = loss
    self.learningRate = learningRate
    self.decayRate = decayRate
    self.initialDecay = decayRate
    self.id = id
  }

  func train(layers: [Layer], input: Matrix<Double>, target: Matrix<Double>) {

    // Make sure cache has been set
    if cache.count != layers.count {
      initializeCache(layers: layers)
    }

    let (signals, gradients) = forward(layers: layers, input: input)
    guard let out = signals.last else { return }
    let loss = self.loss.prime(input: out, target: target).T
    backward(layers: layers, inputs: signals, grads: gradients, loss: loss, batchSize: out.rows)
  }

  func initialize(iterations: Int, cache: [[Matrix<Double>]]) {
    self.iterations = iterations
    self.cache = cache
  }

  fileprivate func initializeCache(layers: [Layer]) {
    cache = layers.map {
      [Matrix<Double>(repeating: 0.0, rows: $0.inputSize + ($0.useBias ? 1 : 0), columns: $0.outputSize)]
    }
  }

  fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    return -learningRate * gradient
  }

  fileprivate func getLearningRate() -> Double {
    guard initialDecay > 0.0 else { return learningRate }
    return learningRate * (1.0 / (1.0 + decayRate * Double(iterations)))
  }

  private func forward(layers: [Layer], input: Matrix<Double>) -> ([Matrix<Double>], [Matrix<Double>]) {
    var output = Matrix(copy: input)
    // Collection of derivatives of activation functions
    var derivatives: [Matrix<Double>] = []
    // Passed through activation function
    var outputs = [output]
    // Loop over layers and calculate output
    for layer in layers {
      let signal = layer.forward(input: output)
      output = layer.activate(input: signal, withGradient: false)
      outputs.append(output)
      // Derivative used for weight update
      let derivative = layer.activate(input: signal, withGradient: true)
      derivatives.append(derivative.T)
    }
    return (outputs, derivatives)
  }

  private func backward(layers: [Layer], inputs: [Matrix<Double>], grads: [Matrix<Double>], loss: Matrix<Double>, batchSize: Int) {
    let lr = getLearningRate()
    iterations += 1
    let layerCount = layers.count
    var loss = loss
    for i in stride(from: layerCount - 1, to: -1, by: -1) {
      // Calculate the delta from previous 'layer'
      let Δ = loss × grads[i]  // Element-wise multiplication
      // Should we add a bias term?
      let temp = layers[i].useBias ? addBias(input: inputs[i]) : inputs[i]
      // Compute change from gradient...
      let grad = ((Δ * temp) / Double(batchSize)).T
      // Calculate weight change
      let ΔW = computeDelta(index: i, learningRate: lr, gradient: grad)
      // Update layer weights
      layers[i].update(delta: ΔW)
      // Update loss for subsequent layer
      loss = layers[i].loss(input: Δ)
    }
  }
}

class SGDMomentum: SGD {
  let momentum: Double
  let nesterov: Bool

  init(loss: DifferentiableLoss, learningRate: Double = 0.01, decayRate: Double = 0.0, momentum: Double = 0.9, withNesterov nesterov: Bool = true) {
    self.momentum = momentum
    self.nesterov = nesterov
    super.init(loss: loss, learningRate: learningRate, decayRate: decayRate)
  }

  override fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    let v = momentum * cache[index][0] - learningRate * gradient  // Velocity
    // Update delta in cache
    cache[index][0] = v
    if nesterov {
      return momentum * v - learningRate * gradient
    }
    return v
  }
}

/// RMSProp optimizer.
class RMSProp: SGD {
  /// Memory parameter. Must be >= 0.0. Defaults to 0.9
  let rho: Double
  /// Fuzz factor.
  let ε: Double
  /// It is recommended to leave the parameters of this optimizer at their
  /// default values (except the learning rate, which can be freely tuned).
  /// This optimizer is usually a good choice for recurrent neural networks
  ///
  /// - Parameters:
  ///   - loss: Input loss function object
  ///   - learningRate: Learning rate. Must be >= 0.0. Defaults to 0.001
  ///   - rho: Memory parameter. Must be >= 0.0. Defaults to 0.9
  ///   - ε: Fuzz factor.
  ///   - decayRate: Learning rate decay over each update.
  /// - References:
  ///   - RMSProp: Divide the gradient by a running average of its recent
  ///     magnitude
  ///     http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf
  init(loss: DifferentiableLoss, learningRate: Double = 0.001, decayRate: Double = 0.0, rho: Double = 0.9, ε: Double = 1e-8) {
    self.rho = rho
    self.ε = ε
    super.init(loss: loss, learningRate: learningRate, decayRate: decayRate)
  }

  override fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    let g = pow(gradient, 2.0)
    var new_a = rho * cache[index][0]
    new_a = (new_a + (1.0 - rho) * g)  // Builds faster when on separate lines?!
    cache[index][0] = new_a
    let root = Matrix(sqrt(new_a.grid) + ε, rows: new_a.rows, columns: new_a.columns)
    return (-learningRate * gradient) ÷ root
  }
}

/// Adagrad optimizer.
class Adagrad: SGD {
  /// Fuzz factor.
  let ε: Double
  /// It is recommended to leave the parameters of this optimizer at their
  /// default values (except the learning rate, which can be freely tuned).
  /// This optimizer is usually a good choice for recurrent neural networks
  ///
  /// - Parameters:
  ///   - loss: Input loss function object
  ///   - learningRate: Learning rate. Must be >= 0.0. Defaults to 0.01
  ///   - ε: Fuzz factor.
  ///   - decayRate: Learning rate decay over each update.
  /// - References:
  ///   - Adaptive subgradient methods for online learning and stochastic
  ///     optimization
  ///     http://www.jmlr.org/papers/volume12/duchi11a/duchi11a.pdf
  init(loss: DifferentiableLoss, learningRate: Double = 0.01, decayRate: Double = 0.0, ε: Double = 1e-8) {
    self.ε = ε
    super.init(loss: loss, learningRate: learningRate, decayRate: decayRate)
  }

  override fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    let g = pow(gradient, 2.0)
    let new_a = cache[index][0] + g  //  Update accumulator
    cache[index][0] = new_a
    let root = Matrix(sqrt(new_a.grid) + ε, rows: new_a.rows, columns: new_a.columns)
    return (-learningRate * gradient) ÷ root
  }
}

/// Adadelta optimizer.
class Adadelta: SGD {
  /// Memory parameter. Must be >= 0.0. Defaults to 0.95
  let rho: Double
  /// Fuzz factor.
  let ε: Double
  /// It is recommended to leave the parameters of this optimizer at their
  /// default values (except the learning rate, which can be freely tuned).
  /// This optimizer is usually a good choice for recurrent neural networks
  ///
  /// - Parameters:
  ///   - loss: Input loss function object
  ///   - learningRate: Learning rate. Must be >= 0.0. Defaults to 1.0
  ///   - rho: Memory parameter. Must be >= 0.0. Defaults to 0.9
  ///   - ε: Fuzz factor.
  ///   - decayRate: Learning rate decay over each update.
  /// - References:
  ///   - Adadelta - An adaptive learning rate method
  ///     http://arxiv.org/abs/1212.5701
  init(loss: DifferentiableLoss, learningRate: Double = 1.0, decayRate: Double = 0.0, rho: Double = 0.95, ε: Double = 1e-8) {
    self.rho = rho
    self.ε = ε
    super.init(loss: loss, learningRate: learningRate, decayRate: decayRate)
  }

  override fileprivate func initializeCache(layers: [Layer]) {
    cache = layers.map {
      [Matrix<Double>(repeating: 0, rows: $0.inputSize, columns: $0.outputSize),
       Matrix<Double>(repeating: 0, rows: $0.inputSize, columns: $0.outputSize)]
    }
  }

  override fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    let g = pow(gradient, 2.0)
    // Update accumulator
    var new_a: Matrix<Double> = rho * cache[index][0]
    new_a = (new_a + (1.0 - rho) * g)  // Builds faster when on separate lines?!
    cache[index][0] = new_a
    // Use new accumulator and *old* delta accumulator
    let temp = cache[index][1]
    let root = Matrix(sqrt(temp.grid + ε), rows: temp.rows, columns: temp.columns)
    let update = (root × gradient) ÷ Matrix(sqrt(new_a.grid + ε), rows: new_a.rows, columns: new_a.columns)
    // Update delta accumulator
    let p = pow(update, 2.0)
    let out = rho * cache[index][1]
    cache[index][1] = out + (1.0 - rho) * p
    return -learningRate * update
  }
}

/// Adadelta optimizer.
class Adam: SGD {
  /// Memory parameter. Must be >= 0.0. Defaults to 0.95
  let rho: Double = 0.95
  /// Fuzz factor.
  let ε: Double
  /// Memory parameters. 0 < β < 1. Generally close to 1.
  let β1: Double
  let β2: Double
  /// Default parameters follow those provided in the original paper.
  ///
  /// - Parameters:
  ///   - loss: Input loss function object
  ///   - learningRate: Learning rate. Must be >= 0.0. Defaults to 1.0
  ///   - β1: Memory parameter. 0 < β1 < 1. Generally close to 1.
  ///   - β2: Memory parameter. 0 < β2 < 1. Generally close to 1.
  ///   - ε: Fuzz factor.
  ///   - decayRate: Learning rate decay over each update.
  /// - References:
  ///   - Adam - A method for stochastic optimization
  ///     http://arxiv.org/abs/1412.6980v8
  init(loss: DifferentiableLoss, learningRate: Double = 1.0, decayRate: Double = 0.0, β1: Double = 0.9, β2: Double = 0.999, ε: Double = 1e-8) {
    self.β1 = β1
    self.β2 = β2
    self.ε = ε
    super.init(loss: loss, learningRate: learningRate, decayRate: decayRate)
  }

  override fileprivate func initializeCache(layers: [Layer]) {
    cache = layers.map {
      [Matrix<Double>(repeating: 0, rows: $0.inputSize, columns: $0.outputSize),
       Matrix<Double>(repeating: 0, rows: $0.inputSize, columns: $0.outputSize)]
    }
  }

  override fileprivate func getLearningRate() -> Double {
    let lr = super.getLearningRate()
    let t = Double(iterations + 1)
    return lr * (sqrt(1.0 - pow(β2, t)) / (1.0 - pow(β1, t)))
  }

  override fileprivate func computeDelta(index: Int, learningRate: Double, gradient: Matrix<Double>) -> Matrix<Double> {
    let g2 = pow(gradient, 2.0)
    var m_t = β1 * cache[index][0]
    m_t = (m_t + (1.0 - β1) * gradient)
    var v_t = β2 * cache[index][1]
    v_t = (v_t + (1.0 - β1) * g2)
    // Update accumulators
    cache[index][0] = m_t
    cache[index][1] = v_t
    let root = Matrix(sqrt(v_t.grid) + ε, rows: v_t.rows, columns: v_t.columns)
    return (-learningRate * m_t) ÷ root // Element-wise division
  }
}

/// Add a bias value of 1
///
/// The value of the bias is adjusted through weights rather than modifying
/// the input signal.
///
/// - Parameter input: Input matrix to which a bias column should be added
/// - Returns: Output matrix with bias column added at 0
fileprivate func addBias(input: Matrix<Double>) -> Matrix<Double> {
  let bias = Matrix<Double>(repeating: 1, rows: input.rows, columns: 1)
  var out = Matrix<Double>(repeating: 0, rows: input.rows, columns: input.columns + 1)
  out[0..<out.rows, 0...0] = bias[0..<bias.rows, 0...0]  // Add bias at 0
  out[0..<out.rows, 1..<out.columns] = input[0..<input.rows, 0..<input.columns]  // All A
  return out
}
