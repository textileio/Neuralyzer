import Foundation

/// Linear stack of layers
class Sequential {
  /// Array of layers contained within this model
  private(set) var layers: [Layer] = []
  /// Optimizer to use
  private(set) var optimizer: Optimizer?
  /// Size (number of features) of incoming data 'layer'
  // TODO: Remove this in favor of an Input layer?
  let inputSize: Int

  /// Initialize a sequential artificial neural network
  ///
  /// - Parameter inputSize: Size of input data to be fed into the network
  init(inputSize: Int, optimizer: Optimizer? = nil) {
    self.inputSize = inputSize
    self.optimizer = optimizer
  }

  /// Adds a layer instance on top of the layer stack
  ///
  /// - Parameter layer: Layer instance/object
  func add(layer: Layer, id: String = UUID().uuidString, weights: Matrix<Double>? = nil) {
    layer.initialize(inputSize: layers.last?.outputSize ?? inputSize, id: id, weights: weights)
    layers.append(layer)
  }

  /// Generates output predictions for the input samples
  ///
  /// - Parameter input: The input data, as a Swift array
  /// - Returns: A Swift array of predictions
  public func predict(input: [[Double]]) -> [[Double]] {
    let output = predict(input: Matrix<Double>(unpack: input))
    var result: [[Double]] = []
    for i in 0..<output.rows {
      result.append(output[row: i].map { Double($0) })
    }
    return result
  }

  /// Generates output predictions for the input samples
  ///
  /// - Parameter input: The input data matrix
  /// - Returns: A Matrix of predictions
  public func predict(input: Matrix<Double>) -> Matrix<Double> {
    var output = Matrix(copy: input)
    for layer in layers {  // Loop over network layers and accumulate output
      output = layer.activate(input: layer.forward(input: output), withGradient: false)
    }
    return output
  }

  /// Trains the model by trampolining to the stored optimizer
  ///
  /// - Parameter input: The input data matrix
  /// - Parameter target: The validated target matrix
  public func train(input: Matrix<Double>, target: Matrix<Double>) {
    guard let optimizer = optimizer else { return }
    optimizer.train(layers: layers, input: input, target: target)
  }
}
