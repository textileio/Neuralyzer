import Foundation

/// Any 'function' that takes an input Matrix and returns an output Matrix
protocol DifferentiableActivation: Specable {
  /// Main function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double>
  /// Gradient/derivative of function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of activation function
  func prime(input: Matrix<Double>) -> Matrix<Double>
}

protocol LeakyDifferentiableActivation: DifferentiableActivation {
  var alpha: Double { get }
}

/// Softmax activation
struct Softmax: DifferentiableActivation {
  /// Softmax activation function
  /// References:
  /// http://eli.thegreenplace.net/2016/the-softmax-function-and-its-derivative/
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    // Exponentiation can be unstable in some cases (i.e., maximal
    // representable Double in Swift is on the order of 10^308)...
    return apply(rowSoftmax, to: input, along: .rows)
  }

  /// Gradient/derivative of activation function
  /// f(x)' = f(x)(δ - f(x)), or f(x)(1 - f(x)) for i = j and -f(x)^2 for i ≠ j
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    //    let signal = fx(input)
    //    let δ = kron(rows: input.rows, columns: input.columns)
    //    return signal × (δ - signal)  // Element-wise multiplication
    return Matrix<Double>(repeating: 1, rows: input.rows, columns: input.columns)
  }
}

// Exponential linear unit (ELU)
struct ELU: LeakyDifferentiableActivation {
  /// Parameter to control 'leakiness' of linear unit
  let alpha: Double

  init(alpha: Double = 1.0) {
    self.alpha = alpha
  }

  /// Exponential linear unit (ELU) activation function
  /// f(α, x) = α * (exp(x) - 1) for x < 0, f(x) = x for x >= 0
  ///
  /// References:
  /// https://arxiv.org/abs/1511.07289v3
  /// https://arxiv.org/abs/1511.07289v1
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid.map { $0 < 0.0 ? (exp($0) - 1.0) * alpha : $0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Derivative of softmax exponential linear unit
  /// f(α, x)' = f(x) + α for x < 0, 1 for x ≥ 0
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let signal = fx(input: input)
    let elements = zip(input.grid, signal.grid)
      .map { $0 < 0.0 ? $1 + alpha : 1.0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Softplus activation
struct Softplus: DifferentiableActivation {
  /// Softplus activation function
  /// f(x) = ln(1 + exp(x))
  ///
  /// References:
  /// http://proceedings.mlr.press/v15/glorot11a/glorot11a.pdf
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = log(exp(input.grid) + 1.0)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  /// f(x)' = 1 / (1 + exp(-x))
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = exp(input.grid) / (exp(input.grid) + 1.0)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Softsign activation
struct Softsign: DifferentiableActivation {
  /// Softsign activation function
  /// f(x) = x / (1 + |x|)
  ///
  /// References:
  /// http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid / (abs(input.grid) + 1.0)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  // f(x)' = 1 / (1 + |x|)^2
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = rec(pow(abs(input.grid) + 1.0, 2.0))
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Rectified linear unit activation function
struct ReLU: DifferentiableActivation {
  /// Rectified linear unit activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = threshold(input.grid, low: 0.0)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid.map { $0 > 0.0 ? 1.0 : 0.0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// LeakyReLU activation
struct LeakyReLU: LeakyDifferentiableActivation {
  // Parameter to control 'leakiness' of linear unit. Setting
  // α = 0.0 gives a standard ReLU, whereas setting α > 0.0  gives
  // a leaky version of ReLU. This allows a small gradient when the unit is
  // not active.
  let alpha: Double

  init(alpha: Double = 0.01) {
    self.alpha = alpha
  }

  /// Leaky rectified linear unit activation function
  /// f(x) = α * x for x < 0, f(x) = x for x >= 0.
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid.map { $0 < 0.0 ? $0 * alpha : $0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid.map { $0 < 0.0 ? alpha : 1.0 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Hyperbolic tangent (TanH) activation function
struct Tanh: DifferentiableActivation {
  /// Tanh activation function
  /// f(x) = tanh(x) = 2 / (1 + exp(-2x)) - 1
  ///
  /// References:
  /// https://en.wikipedia.org/wiki/Hyperbolic_function#Hyperbolic_tangent
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    return Matrix<Double>(tanh(input.grid), rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  /// f(x)' = 1 - f(x)^2
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = neg(pow(fx(input: input).grid, 2.0)) + 1.0
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Sigmoid activation
struct Sigmoid: DifferentiableActivation {
  /// Sigmoid activation function
  /// Also known as logistic and/or soft step activation
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    return Matrix<Double>(expit(input.grid), rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let signal = expit(input.grid)
    return Matrix<Double>(signal * (neg(signal) + 1.0), rows: input.rows, columns: input.columns)
  }
}

// Linear (identity) activation
struct Linear: DifferentiableActivation {
  /// Linear activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    return Matrix(copy: input) // Return the activation signal directly
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    return Matrix<Double>(repeating: 1, rows: input.rows, columns: input.columns)
  }
}

/// Hard sigmoid activation
struct HardSigmoid: DifferentiableActivation {
  /// Approximation of sigmoid (hard sigmoid) activation function
  ///
  /// More approximate and faster than ultra fast sigmoid
  /// Approx in 3 parts: 0, scaled linear, 1.
  /// Removing the slope and shift does not make it faster.
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let elements = clip(input.grid * 0.2 + 0.5, low: 0.0, high: 1.0)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let elements = input.grid.map { $0 < -2.5 || $0 > 2.5 ? 0.0 : 0.2 }
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Elliot activation
struct Elliot: LeakyDifferentiableActivation {
  let alpha: Double

  init(alpha: Double = 0.01) {
    self.alpha = alpha
  }

  /// Elliot activation function
  ///
  /// A fast approximation of sigmoid
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let signal = abs(input.grid * alpha) + 1.0
    let elements = (input.grid * alpha) * 0.5 / signal + 0.5
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let signal = abs(input.grid * alpha) + 1.0
    let elements = pow(signal, 2.0) * (1 / alpha * 0.5)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

// Symmetric elliot activation
struct SymmetricElliot: LeakyDifferentiableActivation {
  let alpha: Double

  init(alpha: Double = 0.01) {
    self.alpha = alpha
  }

  /// Symmetric elliot activation function
  ///
  /// A fast approximation of tanh
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Matrix of same dimension as input
  func fx(input: Matrix<Double>) -> Matrix<Double> {
    let signal = abs(input.grid * alpha) + 1.0
    let elements = (input.grid * alpha) / signal
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }

  /// Gradient/derivative of activation function
  ///
  /// - Parameters:
  ///   - input: Input Matrix of arbitrary dimension
  /// - Returns: Partial derivative of loss function
  func prime(input: Matrix<Double>) -> Matrix<Double> {
    let signal = abs(input.grid * alpha) + 1.0
    let elements = pow(signal, 2.0) * (1 / alpha)
    return Matrix<Double>(elements, rows: input.rows, columns: input.columns)
  }
}

fileprivate func rowSoftmax(_ row: Vector<Double>) -> Vector<Double> {
  let ex = exp(row - max(row))
  return ex / sum(ex)
}
