import Foundation
import XCTest
@testable import Neuralyzer

func getStandardValues() -> Matrix<Double> {
  return Matrix<Double>([0.0, 0.1, 0.5, 0.9, 1.0], rows: 1, columns: 5)
}

func allEqual(actual: Vector<Double>, expected: Vector<Double>, withAccuracy: Double) {
  // swiftlint:disable:next unused_enumerated
  for (i, _) in actual.enumerated() {
    XCTAssertEqual(actual[i], expected[i], accuracy: withAccuracy)
  }
}

class ActivationTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    super.tearDown()
  }

  func testSoftmax() {
    func softmax(_ values: Vector<Double>) -> Vector<Double> {
      let m = max(values)
      let e = exp(values - m)
      return e / sum(e)
    }
    let testValues = getStandardValues()
    let result = Softmax().fx(input: testValues).grid
    let expected = softmax(testValues.grid)
    allEqual(actual: result, expected: expected, withAccuracy: 1e-05)
  }

  func testSigmoid() {
    func baseSigmoid(_ x: Double) -> Double {
      if x >= 0 {
        return 1.0 / (1.0 + exp(-x))
      } else {
        let z = exp(x)
        return z / (1 + z)
      }
    }
    let testValues = getStandardValues()
    let result = Sigmoid().fx(input: testValues).grid
    let expected = Vector(testValues.grid.map(baseSigmoid))
    allEqual(actual: result, expected: expected, withAccuracy: 1e-05)
  }

}
