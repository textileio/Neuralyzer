import Foundation
import XCTest
@testable import Neuralyzer

class NeuralyzerTests: XCTestCase {
    
  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    super.tearDown()
  }

  func testTrainXOR() {
    // Should get high accuracy on basic XOR test
    let X = Matrix(unpack: [[0.0, 0.0], [0.0, 1.0], [1.0, 0.0], [1.0, 1.0]])
    let y = Matrix(unpack: [[0.0], [1.0], [1.0], [0.0]])
    let layers: [Layer] = [Dense(units: 4, activation: Sigmoid()),
                           Dense(units: 1, activation: Sigmoid())]
    let net = Sequential(inputSize: 2, optimizer: RMSProp(loss: BinaryCrossentropy(), learningRate: 0.03, rho: 0.99))
    for layer in layers {
      net.add(layer: layer)
    }

    let epochs = 1000
    for epoch in 0...epochs {
      net.train(input: X, target: y)
      let estimate = net.predict(input: X)  // Grab estimates
      let accuracy = evaluate(estimate: estimate, target: y)
      let l = net.optimizer!.loss.fx(input: estimate, target: y)
      if epoch%100 == 0 {
        print("Epoch \(epoch+1):\(epochs) - loss \(l) - accuracy \(accuracy)")
      }
    }
    let estimate = net.predict(input: X)
    XCTAssertGreaterThan(quality(estimate: estimate, target: y, loss: net.optimizer!.loss), 0.99)
    XCTAssertEqual(evaluate(estimate: estimate, target: y), 1.0)
  }

  func testModelSpecification() {
    let layers: [Layer] = [
      Dense(units: 8, activation: ELU(), kernelInitializer: GlorotUniform(), biasInitializer: Zeros()),
      Dense(units: 2, activation: Softmax(), kernelInitializer: GlorotUniform(), biasInitializer: Zeros())
    ]
    let net = Sequential(inputSize: 4, optimizer: SGDMomentum(loss: CategoricalCrossentropy()))
    for layer in layers {
      net.add(layer: layer)
    }
    var json = net.spec
    json["training"] = ["optimizer": net.optimizer!.spec, "loss": net.optimizer!.loss.spec]
    let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
    let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
    print(string!)
  }

  func testBasicModel() {
    // Create some data...
    let nrows = 20
    let vector = Vector<Double>(initializedWith: uniform, count: nrows * 4)
    let data = Matrix<Double>(vector, rows: nrows, columns: 4)
    // Setup the model architecture...
    // Models are responsible for structuring layers and prediction...
    // That's it!
    let layers: [Layer] = [Dense(units: 2, activation: ReLU()),
                           Dense(units: 1, activation: Sigmoid())]
    // Create model with a loss function and an optimizer..
    let net = Sequential(inputSize: 4, optimizer: SGD(loss: MeanSquaredError()))
    for layer in layers {
      net.add(layer: layer)
    }

    // Now we start the training process...
    let epochs = 20
    for epoch in 1..<epochs {
      let t0 = Date()
      for i in 0..<data.rows {
        let X = Matrix<Double>(unpack: [data[row: i].map{Double($0)}])
        let Y = Matrix<Double>(unpack: [[uniform()]])
        net.train(input: X, target: Y)
      }
      print("Epoch \(epoch)/\(epochs)")
      print(Date().timeIntervalSince(t0))
    }
  }

  func evaluate(estimate: Matrix<Double>, target: Matrix<Double>) -> Double {
    var running: Double = 0.0
    if target.columns > 1 {
      for i in 0..<target.rows {
        running += (argmax(estimate[row: i]) == argmax(target[row: i])) ? 1.0 : 0.0
      }
    } else {
      //    // Compute an R^2
      //    // Could use rmsq or linregress
      //    let mss = sum(pow(target.elements - mean(target.elements), 2.0))
      //    let sse = sum(pow(estimate.elements - target.elements, 2.0))
      //    accuracy = 1.0 - (mss / sse)
      for i in 0..<target.rows {
        running += (round(estimate[row: i]) == target[row: i] ? 1.0 : 0.0)
      }
    }
    return running / Double(target.rows)
  }

  func quality(estimate: Matrix<Double>, target: Matrix<Double>, loss: DifferentiableLoss) -> Double {
    let meanError = loss.fx(input: estimate, target: target) / Double(target.rows)
    // Calculate the numeric range between the minimum and maximum output value
    let range = max(estimate.grid) - min(target.grid)
    // Return the measured quality
    return 1.0 - (meanError / range)
  }

}
